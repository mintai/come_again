from info import create_app,rs
from flask_script import Manager
from config import DevelopMode

app = create_app(DevelopMode)
manager = Manager(app)

if __name__ == '__main__':
    print(app.url_map)
    manager.run()
