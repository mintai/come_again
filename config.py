import logging


class Config(object):
    SQLALCHEMY_DATABASE_URI = "mysql://root:123456@127.0.0.1:3306/xj_info"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    SECRET_KEY = "zhangliang"

    SESSION_TYPE = "redis"
    SESSION_USE_SIGNER = True


class DevelopMode(Config):
    DEBUG = True
    LOG_LEVEL = logging.DEBUG


class ProductMode(Config):
    DEBUG = False
    LOG_LEVEL = logging.ERROR
