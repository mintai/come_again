import logging
from logging.handlers import RotatingFileHandler

import redis
from flask import Flask
from flask_script import Manager
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from flask_wtf.csrf import generate_csrf

from config import Config

db = SQLAlchemy()
rs = None  # type: redis.StrictRedis




def setup_log(config_mode):
    logging.basicConfig(level=config_mode.LOG_LEVEL)
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    formatter = logging.Formatter("%(levelname)s %(filename)s %(lineno)d %(message)s")
    file_log_handler.setFormatter(formatter)
    logging.getLogger().addHandler(file_log_handler)


def create_app(config_mode):
    setup_log(config_mode)
    app = Flask(__name__)

    # 导入配置信息
    app.config.from_object(Config)

    global db, rs
    # 为数据库mysql与redis重新赋值
    db.init_app(app)
    rs = redis.StrictRedis(host=config_mode.REDIS_HOST, port=config_mode.REDIS_PORT, decode_responses=True)


    Session(app)
    # 增加csrf保护,以后遇到POST PUT DELETE请求,服务器会自动获取csrf,并进行对比
    CSRFProtect(app)

    @app.after_request
    def after_request(response):
        csrf_token = generate_csrf()
        response.set_cookie("csrf_token", csrf_token)
        return response

    from info.utils.common import do_index_class
    app.add_template_filter(do_index_class, "rank_class")

    from info.modules.index import index_blue
    app.register_blueprint(index_blue)

    from info.modules.passport import passport_blue
    app.register_blueprint(passport_blue)

    from info.modules.news import news_blue
    app.register_blueprint(news_blue)

    return app
